import { Component, OnInit } from '@angular/core';
import 'socket.io-client';
declare let io: any;


class message {
    text: string;
    name: string;

    constructor(text: string, name: string) {
        this.text = text;
        this.name = name;
    }
}

@Component({
    selector: 'chat-app',
    templateUrl: `components/chat.component/chat.component.html`,
    styleUrls: ['components/chat.component/css/chat.component.css']
})



export class ChatComponent {
    text_message: string = "";
    socket: any = io('localhost:3000');
    messages: message[] = [];
    disabl: boolean = true;
    error: string = "";


    ngOnInit() {
        localStorage.clear();
        var ref = this;
        this.socket.on('updateSocketList', function (data) {
            console.log(data);
            ref.messages = [];
        })
        this.socket.on('addUserToSocketList', function (data) {
            console.log('user add to chat ' + data);
        })

        this.socket.on('mes_story', function (data) {
            console.log(data);
            ref.messages.push(new message(data.text, data.name));
        })
    }

    connect(name, room): void {
        this.error = "";
        console.log(localStorage.getItem('user'));
        if (localStorage.getItem('user') !== null) {
            var user = JSON.parse(localStorage.getItem('user'));
            if (room === user.room) {
                this.error = "You are here";
            }
        }


        this.socket.emit('user', { name: name, room: room })
        this.disabl = false;
        this.messages = [];
        localStorage.setItem('user', JSON.stringify({ name: name, room: room }));
    }



    private sendMessage(value): void {
        this.socket.emit('chatMessage', value)
        this.text_message = "";
    }

    private(name, room): void {

    }

}