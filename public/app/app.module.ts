import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from "./app.component";
import { ChatComponent } from "./components/chat.component/chat.component";

const appRoutes: Routes = [
    { path: '', component: ChatComponent },
];

@NgModule({
    imports: [BrowserModule, FormsModule, RouterModule.forRoot(appRoutes)],
    declarations: [AppComponent, ChatComponent],
    providers: [],
    bootstrap: [AppComponent]
})

export class AppModule { }