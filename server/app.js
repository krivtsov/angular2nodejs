var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app = express();

let clientListNames = [];

var http = require('http');
var server = http.createServer(app);
var io = require('socket.io').listen(server);


var api = require('./function.js');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '/../', 'public')));
app.use(express.static(path.join(__dirname, '/../', 'public', 'app')));
app.use(express.static(path.join(__dirname, '/../', 'node_modules')));
app.use(express.static(path.join(__dirname, '/../')));



//catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

io.on('connection', function (socket) {
    console.log('connection');
    api.joinRoom(socket, 'room');

    socket.on('user', function (data, fn) {
        clientListNames.push(data.name);

        socket.leave('room');

        api.joinRoom(socket, data.room);



        api.nickNames[socket.id] = data.name;
        api.currentRoom[socket.id] = data.room;

        io.emit('updateSocketList', clientListNames);
        io.emit("addUserToSocketList", data.name);
    })
    .on('chatMessage', function(data){
        io.sockets.in(api.currentRoom[socket.id]).emit('mes_story', {name: api.nickNames[socket.id], text: data});
    })
        // .on('disconnect', function () {
        //     let name = socket.handshake.query.userName;
        //     let userIndex = clientListNames.indexOf(socket.handshake.query.userName);
        //     if (userIndex != -1) {
        //         clientListNames.splice(userIndex, 1);
        //         io.emit("updateSocketList", clientListNames);
        //         io.emit("removeUserFromSocketList", name);
        //     }
        // });
})


server.listen(3000, function () {
    console.log('server run is ' + 3000);
});




module.exports = app;